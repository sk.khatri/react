import React from 'react'
import ReactDom from 'react-dom'
// CSS
import './index.css'
// Stateless functional component
// always returns JSX

// function Greeting() {
//   return <h4>this is Sunil this is my first react app</h4>
// }

const Greeting = () => {
  return React.createElement(
    'h1',
    {},
    'hello Sunil. This is my first React app.'
  )
}

const Person = () => {
  return <h2> SUNIL KHATRI </h2>
}

const Message = () => {
  return <h4> How are you doing ? </h4>
}

const title =
  'If Animals Kissed Good Night Board book – Illustrated, June 3, 2014'
const author = 'Ann Whitford Paul'
const imagePath =
  'https://images-na.ssl-images-amazon.com/images/I/51PRQuO-xjL._SY498_BO1,204,203,200_.jpg'

function Greeting2() {
  return (
    <div>
      <Person />
      <Message />
    </div>
  )
}

//const Book = (props) => {
function Book(props) {
  console.log(props)
  return (
    <article className='book'>
      <img src={props.imgPath} alt='' />
      <h1>{props.title}</h1>
      <h4>by - {props.author.toUpperCase()}</h4>
    </article>
  )
}
function BookList() {
  return (
    <section className='booklist'>
      <Book imgPath={imagePath} author={author} title={title} />
      <Book imgPath={imagePath} author={author} title={title} />
      <Book imgPath={imagePath} author={author} title={title} />
      <Book imgPath={imagePath} author={author} title={title} />
      <Book imgPath={imagePath} author={author} title={title} />
      <Book imgPath={imagePath} author={author} title={title} />
      <Book imgPath={imagePath} author={author} title={title} />
    </section>
  )
}

// ReactDom.render(<Greeting2 />, document.getElementById('root'))
// ReactDom.render(<Greeting />, document.getElementById('root'))
ReactDom.render(<BookList />, document.getElementById('root'))
